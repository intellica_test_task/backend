const express = require('express');
const router = express.Router();
const UsersController = require( '../controllers/users.ctrl.js');
const UsrCtrl = new UsersController();

UsrCtrl.on('error', console.log);

var userList = [
    {
        id: "1",
        name: "guy",
        role: "admin"
    },
    {
        id: "2",
        name: "avior",
        role: "editor"
    },
    {
        id: "3",
        name: "fill",
        role: "editor"
    },
    {
        id: "4",
        name: "voicespin",
        role: "admin"
    },
    {
        id: "5",
        name: "11",
        role: "admin"
    }
];

function respond(status, res, data) {
    res.status(status).send(JSON.stringify(data));
}

router.get('/get', (req, res)  => {
    UsrCtrl.getAllUsers(userList, (data) => {
        respond(200, res, data);
    });
});

router.get('/get/:id', (req, res)  => {
    UsrCtrl.getUserById(req.params.id, userList, (data) => {
        respond(200, res, data);
    });
});

router.route('/add').post( (req, res) => {
    UsrCtrl.createNewUser(req.body, userList, (data) => {
        respond(200, res, data);
    })
});

router.route('/update').put( (req, res) => {
    UsrCtrl.updateUser(req.body, userList, (data) => {
        respond(200, res, data);
    })
});

router.route('/remove').delete((req, res) => {
    UsrCtrl.removeUser(req.body.userId, userList, (data) => {
        respond(200, res, data);
    })
});

module.exports = router;
