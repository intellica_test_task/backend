const EventEmitter = require('events');

class UsersController extends EventEmitter {
    constructor() {
        super();
    }

    getAllUsers(userList, callback) {
        return callback(userList);
    }

    createNewUser(userData, userList, callback) {
        if (userData.name && userData.role) {
            userData.id = userList.length+1+Date.now().toString();
            userList.push(userData);
            return callback(userData);
        }
        else this.emit('error', 'Invalid Data');
        return callback(false);
    }

    updateUser(updateData, userList, callback) {
        if (updateData.userId && (updateData.name || updateData.role)) {
            try {
                for(let user of userList){
                    if(user.id == updateData.userId) {
                        updateData.name ? user.name = updateData.name : {};
                        updateData.role ? user.role = updateData.role : {};
                        return callback(true);
                    }
                }
            }
            catch (error) {
                this.emit('error', error);
                return callback(false);
            }
        }
        else this.emit('error', 'Invalid Data');
        return callback(false);
    }

    removeUser(userId, userList, callback) {
        if (userId) {
            try {
                for(let user in userList){
                    if(userList[user].id == userId) {
                        userList.splice(user, 1);
                        return callback(true);
                    }
                }
            }
            catch (error) {
                this.emit('error', error);
                return callback(false);
            }
        }
        else this.emit('error', 'Invalid Data');
        return callback(false);
    }

    getUserById(userId, userList, callback) {
        if (userId) {
            try {
                for(let user in userList){
                    if(userList[user].id == userId) {
                        return callback(userList[user]);
                    }
                }
            }
            catch (error) {
                this.emit('error', error);
                return callback(false);
            }
        }
        else this.emit('error', 'Invalid Data');
        return callback(false);
    }

}

module.exports = UsersController;